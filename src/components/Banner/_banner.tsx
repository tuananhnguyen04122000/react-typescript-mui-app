import { Button, Grid, Typography } from '@mui/material';
import Box from '@mui/material/Box';
import { height } from '@mui/system';
import React, { FC } from 'react'
import { Link } from 'react-router-dom';
import Slider, { Settings } from 'react-slick';
import { IProductBanner } from '.'

interface BannerProps {
    productBanners: IProductBanner[],
}

const Banner: FC<BannerProps> = (props) => {
    const { productBanners } = props;

    const settings = {
        dots: true,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        className: "slick-track-banner",
    };
    return (
        <Slider {...settings} arrows={false} >
            {productBanners.map((productBannersItem, index) => {
                return (
                    <>
                        <Grid container spacing={3} sx={{ height: 400, justifyContent: "center", }}>
                            <Grid display={"flex"} item xs={12} sm={5} alignItems={"center"}>
                                <Box sx={{ cursor: "text" }}>
                                    <Typography
                                        variant="h1"
                                        sx={{
                                            height: 120,
                                            fontSize: 50,
                                            lineHeight: 1.2,
                                            mb: "1.35rem",
                                            fontWeight: "bold"
                                        }}>
                                        {productBannersItem.title}
                                    </Typography>
                                    <Typography
                                        sx={{ fontSize: 14, mb: "21.6px" }}> {productBannersItem.content} </Typography>
                                    <Link to={"/"} style={{ textDecoration: "none" }}>
                                        <Button variant="contained" color="error" size="large">Shop Now</Button>
                                    </Link>
                                </Box>
                            </Grid>
                            <Grid item xs={12} sm={5}>
                                <img style={{ maxWidth: "80%", marginLeft: "auto", marginRight: "auto" }} src={`${productBannersItem.imgUrl}`} />
                            </Grid>
                        </Grid>
                    </>
                )
            })}
        </Slider>
    )
}

export default Banner
