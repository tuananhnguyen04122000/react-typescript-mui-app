import { Box, Card, CardContent, Typography } from '@mui/material'
import React, { FC } from 'react'
import { ICompanyCar } from '.'

interface PropsCopanyCar {
    companyCars: ICompanyCar[]
}

const CompanyCar: FC<PropsCopanyCar> = (props) => {
    const { companyCars } = props

    const styleCardContentImage = {
        display: "flex",
        width: 168,
        height: 20,
        mb: "12px",
        py: "12px",
        bgcolor: "#F6F9FC",
        color: "#2B3445",
        borderRadius: "5px",
        gap: "1rem",
        alignItems: "center",
        "&:last-child": { paddingBottom: "12px" }
    }

    return (
        <Card sx={{ minWidth: 240, height: 472 }}>
            <CardContent
                sx={{
                    padding: "20px",
                    "&:last-child": { paddingBottom: "20px" }
                }}>
                {companyCars.map((companyCarItem, index) => {
                    return (
                        <CardContent key={`${companyCarItem.name}-${index}`} sx={styleCardContentImage}>
                            <Box sx={{ width: 20, height: 20 }}>
                                <img style={{ width: "100%", height: "100%" }} src={companyCarItem.imgUrl} />
                            </Box>
                            <Typography variant="h4" sx={{ fontSize: 17, fontWeight: 600, lineHeight: 1, textTransform: "capitalize" }}>{companyCarItem.name}</Typography>
                        </CardContent>
                    )
                })}
                <CardContent
                    sx={{
                        width: 168,
                        height: 20,
                        bgcolor: "#F6F9FC",
                        borderRadius: "5px",
                        pt: "12px",
                        "&:last-child": { paddingBottom: "12px" },
                        mt: "64px"
                    }}>
                    <Typography variant="h4" sx={{ fontSize: 17, fontWeight: 600, lineHeight: 1, textTransform: "capitalize", textAlign: "center" }}>View All Brands</Typography>
                </CardContent>
            </CardContent>
        </Card>
    )
}

export default CompanyCar
