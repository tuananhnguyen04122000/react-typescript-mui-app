import { Box, Card, Container, Grid, Typography } from '@mui/material'
import { Link } from 'react-router-dom'
import { IProductFeaturedBrand as IProductCategories } from "../TopRatingAndFeaturedBrands"

import React from 'react'
import WidgetsIcon from '@mui/icons-material/Widgets';
import ArrowRightIcon from '@mui/icons-material/ArrowRight';

const Index = () => {
    const productsCategories: IProductCategories[] = [
        {
            imgUrl: "https://bazaar.ui-lib.com/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FFashion%2FClothes%2F7.DenimClassicBlueJeans.png&w=64&q=75", name: "Automobile", id: 1
        },
        {
            imgUrl: "https://bazaar.ui-lib.com/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FFashion%2FClothes%2F20.GrayOvercoatWomen.png&w=64&q=75", name: "Car", id: 2
        },
        {
            imgUrl: "https://bazaar.ui-lib.com/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FFashion%2FJewellery%2F8.IndianPearlThreadEarrings.png&w=64&q=75", name: "Fashion", id: 3
        },
        {
            imgUrl: "https://bazaar.ui-lib.com/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FFashion%2FJewellery%2F21.FeathersandBeadsBohemianNecklace.png&w=64&q=75", name: "Mobile", id: 4
        },
        {
            imgUrl: "https://bazaar.ui-lib.com/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FFashion%2FShoes%2F11.Flowwhite.png&w=64&q=75", name: "Laptop", id: 5
        },
        {
            imgUrl: "https://bazaar.ui-lib.com/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FGroceries%2F1.SaktiSambarPowder.png&w=64&q=75", name: "Desktop", id: 6
        },
        {
            imgUrl: "https://bazaar.ui-lib.com/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FGroceries%2F14.ACIProducts.png&w=64&q=75", name: "Tablet", id: 7
        },
        {
            imgUrl: "https://bazaar.ui-lib.com/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FGroceries%2F27.SardinesPack.png&w=64&q=75", name: "Fashion", id: 8
        },
        {
            imgUrl: "https://bazaar.ui-lib.com/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FHealth%26Beauty%2F12.BeautySocietyantiacnemask.png&w=64&q=75", name: "Electronics", id: 9
        },
        {
            imgUrl: "https://bazaar.ui-lib.com/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FHealth%26Beauty%2F25.MarioBadescuSkinCareShampoo.png&w=64&q=75", name: "Furniture", id: 10
        },
        {
            imgUrl: "https://bazaar.ui-lib.com/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FHome%26Garden%2F13.GardenRosesinBlueVase.png&w=64&q=75", name: "Camera", id: 11
        },
        {
            imgUrl: "https://bazaar.ui-lib.com/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FFashion%2FAccessories%2F12.Xiaomimiband2.png&w=64&q=75", name: "Electronics", id: 12
        },
    ]

    return (
        <Container maxWidth={false} sx={{ width: 1280, height: 225, mb: "70px" }}>
            <Box sx={{ display: "grid", width: 1232, height: 25, mb: "24px", gridTemplateAreas: `"big-discounts view-all"` }}>
                <Box sx={{
                    display: "flex",
                    gridArea: "big-discounts",
                    alignItems: "center",
                    gap: "8px"
                }}>
                    <WidgetsIcon sx={{ color: "#d23f57" }} />
                    <Typography fontSize={25} sx={{ fontWeight: 700, lineHeight: 1 }}>Categories</Typography>
                </Box>
                <Link to={"/"} style={{
                    gridArea: "view-all",
                    height: 21,
                    alignSelf: "center",
                    justifySelf: "end",
                    textDecoration: "none"
                }}>
                    <Box display={"flex"} alignItems={"center"}>
                        <Typography fontSize={14}>View all</Typography>
                        <ArrowRightIcon fontSize="inherit" />
                    </Box>
                </Link>
            </Box>

            <Grid container spacing={3} sx={{ mt: "-24px", ml: "-24px" }}>
                {productsCategories.map((productCategoriesItem, index) => {
                    return (
                        <Grid key={`${productCategoriesItem.name}-${index}`} item xs={12} sm={4} md={3} lg={2}>
                            <Link to={"/"} style={{ textDecoration: "none" }}>
                                <Card sx={{ display: "flex", p: "12px", alignItems: "center" }}>
                                    <Box sx={{ width: 52, height: 52 }}>
                                        <img style={{ width: "100%", height: "100%" }} src={productCategoriesItem.imgUrl} />
                                    </Box>

                                    <Typography
                                        component={"div"}
                                        sx={{
                                            fontSize: 14,
                                            lineHeight: 1.5,
                                            fontWeight: 420,
                                            ml: "10px"
                                        }}>{productCategoriesItem.name}</Typography>
                                </Card>
                            </Link>
                        </Grid>
                    )
                })}
            </Grid>
        </Container>
    )
}

export default Index
