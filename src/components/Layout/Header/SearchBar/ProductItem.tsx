import { Box, Button, IconButton, Typography } from '@mui/material'
import React, { FC } from 'react'
import AddIcon from '@mui/icons-material/Add';
import RemoveIcon from '@mui/icons-material/Remove';
import CloseIcon from '@mui/icons-material/Close';
import { Link, useRoutes } from 'react-router-dom'

import { useRecoilState } from 'recoil';
import { cartState, ICart, IProduct } from '../../../../recoil/Atoms';
import { useAddToCart, useRemoveAllProducts, useRemoveProductItemQuantity } from '../../../../recoil/Hooks';


interface IPropsProductItem {
    productItem: ICart
}

const ProductItem: FC<IPropsProductItem> = (props) => {
    const { productItem } = props
    const styleBoxProductItem = {
        display: "flex",
        py: "16px",
        px: "20px",
        alignItems: "center",
        borderBottom: "1px solid #f3f5f9",
        justifyContent: "space-between"
    }

    const styleButton = {
        minWidth: 30.4,
        height: 30.4,
        p: "3px",
        color: "#d23f57",
        borderColor: "#d23f57",
        borderRadius: 300,
        ":hover": {
            borderColor: "#d23f57",
            bgcolor: "#fdf7f8",
        }
    }

    const styleQuantity = {
        fontWeight: 600,
        fontSize: 15,
        lineHeight: 1.5,
        my: "3px"
    }

    const styleBoxImage = {
        width: 76,
        height: 76,
        mx: "16px"
    }

    const styleBoxInfo = {
        width: 133.2,
        overflow: "hidden",
        whiteSpace: "nowrap"
    }

    const styleTextName = {
        fontSize: 14,
        fontWeight: 600,
        textTransform: "none",
        textOverflow: "ellipsis",
        lineHeight: 1.5
    }

    const styleTextListProduct = {
        fontSize: 10,
        color: "grey",
        my: 1
    }

    const styleTextPrice = { fontWeight: 600, fontSize: 14, color: "#d32f2f" }
    const product: IProduct = {
        imgUrl: productItem.imgUrl,
        name: productItem.name,
        price: productItem.price,
        id: productItem.id
    }

   const addToCart = useAddToCart()
   const removeProductItemQuantity = useRemoveProductItemQuantity()
   const removeAllProducts = useRemoveAllProducts()
    return (
        <Box sx={styleBoxProductItem}>
            <Box sx={{ display: "flex", flexDirection: "column", alignItems: "center" }}>
                <Button
                    variant='outlined'
                    sx={styleButton}
                    onClick={() => addToCart(product)}
                >
                    <AddIcon fontSize='small' />
                </Button>
                <Typography variant='h3' sx={styleQuantity}>{productItem.quantity}</Typography>
                <Button
                    variant='outlined'
                    sx={styleButton}
                    disabled={productItem.quantity === 1 ? true : false}
                    onClick={() => removeProductItemQuantity(product)}
                >
                    <RemoveIcon fontSize='small' />
                </Button>
            </Box>

            <Link to={`/product/${productItem.id}`}>
                <Box sx={styleBoxImage}>
                    <img src={productItem.imgUrl} style={{ width: "100%", height: "100%" }} />
                </Box>
            </Link>

            <Box sx={styleBoxInfo}>
                <Link to={`/product/${productItem.id}`} style={{ textDecoration: "none", color: "inherit" }}>
                    <Typography variant='h5' sx={styleTextName}>{productItem.name}</Typography>
                </Link>
                <Typography sx={styleTextListProduct}>{productItem.price} US$ x {productItem.quantity}</Typography>
                <Box >
                    <Typography sx={styleTextPrice}>{productItem.totalPriceQuantity} US$</Typography>
                </Box>
            </Box>

            <IconButton size='small' onClick={() => removeAllProducts(product)}>
                <CloseIcon sx={{ width: 20, height: 20 }} />
            </IconButton>
        </Box>
    )
}

export default ProductItem
