import { Button, IconButton, Typography } from '@mui/material'
import { Box } from '@mui/system'
import React, { FC, useState } from 'react'
import RemoveIcon from '@mui/icons-material/Remove';
import AddIcon from '@mui/icons-material/Add';
import { useRecoilState } from 'recoil';
import { IProduct, shoppingBagState, showCounterState } from '../../../../../recoil/Atoms';
import { useAddToCart, useRemoveProductItemQuantity } from '../../../../../recoil/Hooks';

interface PropsCounter {
    counterQuantityProduct: number | undefined
    product: IProduct
    addToCart: (product: IProduct) => void
    removeProductItemQuantity: (product: IProduct) => void
}

const Counter: FC<PropsCounter> = (props) => {
    const {
        counterQuantityProduct,
        product,
        addToCart,
        removeProductItemQuantity
    } = props
    
    const styleButton = {
        minWidth: 36,
        p: 0,
        color: "#d23f57",
        borderColor: "#d23f57",
        ":hover": {
            borderColor: "#d23f57"
        }
    }

    const styleIcon = {
        width: 20,
        height: 20,
        p: "8px"
    }

    const styleCount = {
        mx: "20px",
        fontWeight: 600,
        fontSize: 20,
        lineHeight: 1.8
    }

    return (
        <Box sx={{ display: "flex" }}>
            <Button variant='outlined' sx={styleButton} onClick={() => removeProductItemQuantity(product)}>
                <RemoveIcon sx={styleIcon} />
            </Button>

            <Typography variant='h3' sx={styleCount}>{counterQuantityProduct}</Typography>

            <Button variant='outlined' sx={styleButton} onClick={() => addToCart(product)}>
                <AddIcon sx={styleIcon} />
            </Button>
        </Box>
    )
}

export default Counter
