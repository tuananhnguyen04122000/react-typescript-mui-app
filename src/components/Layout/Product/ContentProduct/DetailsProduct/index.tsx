import { Button, Grid, Rating, Typography } from "@mui/material"
import { Box } from "@mui/system"
import axios from "axios"
import React, { useEffect, useState } from "react"
import { useParams } from "react-router-dom"
import { useRecoilState, useRecoilValue, useSetRecoilState } from "recoil"
import { cartState, ICart, IProduct, shoppingBagState, showCounterState } from "../../../../../recoil/Atoms"
import { useAddToCart, useRemoveProductItemQuantity } from "../../../../../recoil/Hooks"
import Counter from "./Counter"

const Index = () => {
  const styleGridContainer = {
    mt: "-24px",
    ml: "-24px"
  }

  const styleBoxImage = {
    display: "flex",
    width: "100%",
    height: 300,
    mb: "48px",
    justifyContent: "center"
  }

  const styleImage = {
    width: 300,
    height: "100%"
  }

  const styleTextName = {
    height: 45,
    mb: "8px",
    fontSize: 30,
    fontWeight: 700,
    lineHeight: 1.5,
    textTrandform: "none",
    whiteSpace: "nomal"
  }

  const styleBoxBrand = {
    display: "flex",
    mb: "8px"
  }

  const styleBoxRate = {
    display: "flex",
    gap: 1
  }

  const styleBoxPrice = {
    pt: "8px",
    mb: "24px"
  }

  const styleTextPrice = {
    mb: "4px",
    fontSize: 25,
    fontWeight: 700,
    lineHeight: 1,
    color: "#d23f57",
    textTrandform: "none",
    whiteSpace: "nomal"
  }

  const styleButtonAddToCart = {
    px: "28px",
    py: "10px",
    bgcolor: "#d23f57",
    boxShadow: 0,
    fontWeight: 600,
    textTransform: "none",
    ":hover": {
      bgcolor: "#e3364e",
      boxShadow: 0
    }
  }

  const { id } = useParams();

  const [product, setProduct] = useState<IProduct>({
    imgUrl: "",
    name: "",
    price: 0,
    id: ""
  })

  const [cart, setCart] = useRecoilState(cartState)
  const quantity = cart?.find((productItem) => productItem.id === product.id)?.quantity
  const addToCart = useAddToCart();
  const removeProductItemQuantity = useRemoveProductItemQuantity()

  useEffect(() => {
    scrollToTop();
    fetchData();
    const storedCart = localStorage.getItem('cart')
    storedCart && setCart(JSON.parse(storedCart))
    // const storedCart = localStorage.getItem('cart')
    // console.log({storedCart})
    // if(cart && cart.length>0){

    // }
    // else{
    //   storedCart && setCart(JSON.parse(storedCart))
    // }

  }, [id])

  // useEffect(() => {
  //   const storedCart = localStorage.getItem('cart')
  //   console.log({ storedCart })
  //   if (cart && cart.length > 0) {

  //   }
  //   else {
  //     storedCart && setCart(JSON.parse(storedCart))
  //   }

  // }, [])

  async function fetchData() {
    //const result = await axios.get(`https://6402f090f61d96ac4873acc5.mockapi.io/api/product-flashdeals/${id}`);
    //setProduct(result.data);
    await axios.get('https://6402f090f61d96ac4873acc5.mockapi.io/api/product').then((result) => {
      if (Number(id) > 0 && Number(id) <= 6)
        //flashDeals
        setProduct(result.data[0].flashDeals[Number(id) - 1]);
      else if (Number(id) >
        result.data[0].flashDeals.length
        && Number(id) <=
        result.data[0].flashDeals.length +
        result.data[0].topRatings.length)
        //topRatings
        setProduct(result.data[0].topRatings[Number(id) - (result.data[0].flashDeals.length + 1)]);
      else if (Number(id) >
        result.data[0].flashDeals.length +
        result.data[0].topRatings.length
        && Number(id) <=
        result.data[0].flashDeals.length +
        result.data[0].topRatings.length +
        result.data[0].newArrivals.length)
        //newArrivals
        setProduct(result.data[0].newArrivals[Number(id) - (result.data[0].flashDeals.length + result.data[0].topRatings.length + 1)])
      else if (Number(id) >
        result.data[0].flashDeals.length +
        result.data[0].topRatings.length +
        result.data[0].newArrivals.length
        && Number(id) <=
        result.data[0].flashDeals.length +
        result.data[0].topRatings.length +
        result.data[0].newArrivals.length +
        result.data[0].bigDiscounts.length)
        //bigDiscounts
        setProduct(result.data[0].bigDiscounts[Number(id) - (result.data[0].flashDeals.length + result.data[0].topRatings.length + result.data[0].newArrivals.length + 1)])
      else if (Number(id) >
        result.data[0].flashDeals.length +
        result.data[0].topRatings.length +
        result.data[0].newArrivals.length +
        result.data[0].bigDiscounts.length
        && Number(id) <=
        result.data[0].flashDeals.length +
        result.data[0].topRatings.length +
        result.data[0].newArrivals.length +
        result.data[0].bigDiscounts.length +
        result.data[0].cars.length)
        //cars
        setProduct(result.data[0].cars[Number(id) - (result.data[0].flashDeals.length + result.data[0].topRatings.length + result.data[0].newArrivals.length + result.data[0].bigDiscounts.length + 1)])
      else if (Number(id) >
        result.data[0].flashDeals.length +
        result.data[0].topRatings.length +
        result.data[0].newArrivals.length +
        result.data[0].bigDiscounts.length +
        result.data[0].cars.length
        && Number(id) <=
        result.data[0].flashDeals.length +
        result.data[0].topRatings.length +
        result.data[0].newArrivals.length +
        result.data[0].bigDiscounts.length +
        result.data[0].cars.length +
        result.data[0].mobilePhones.length)
        //mobilePhones
        setProduct(result.data[0].mobilePhones[Number(id) - (result.data[0].flashDeals.length + result.data[0].topRatings.length + result.data[0].newArrivals.length + result.data[0].bigDiscounts.length + result.data[0].cars.length + 1)])

      else if (Number(id) >
        result.data[0].flashDeals.length +
        result.data[0].topRatings.length +
        result.data[0].newArrivals.length +
        result.data[0].bigDiscounts.length +
        result.data[0].cars.length +
        result.data[0].mobilePhones.length
        && Number(id) <=
        result.data[0].flashDeals.length +
        result.data[0].topRatings.length +
        result.data[0].newArrivals.length +
        result.data[0].bigDiscounts.length +
        result.data[0].cars.length +
        result.data[0].mobilePhones.length +
        result.data[0].opticsWatch.length
      )
        //opticsWatch
        setProduct(result.data[0].opticsWatch[Number(id) - (result.data[0].flashDeals.length + result.data[0].topRatings.length + result.data[0].newArrivals.length + result.data[0].bigDiscounts.length + result.data[0].cars.length + result.data[0].mobilePhones.length + 1)])

      else if (Number(id) >
        result.data[0].flashDeals.length +
        result.data[0].topRatings.length +
        result.data[0].newArrivals.length +
        result.data[0].bigDiscounts.length +
        result.data[0].cars.length +
        result.data[0].mobilePhones.length +
        result.data[0].opticsWatch.length
        && Number(id) <=
        result.data[0].flashDeals.length +
        result.data[0].topRatings.length +
        result.data[0].newArrivals.length +
        result.data[0].bigDiscounts.length +
        result.data[0].cars.length +
        result.data[0].mobilePhones.length +
        result.data[0].opticsWatch.length +
        result.data[0].moreForYous.length
      )
        //moreForYous
        setProduct(result.data[0].moreForYous[Number(id) - (result.data[0].flashDeals.length + result.data[0].topRatings.length + result.data[0].newArrivals.length + result.data[0].bigDiscounts.length + result.data[0].cars.length + result.data[0].mobilePhones.length + result.data[0].opticsWatch.length + 1)])
    });
  }

  function scrollToTop() {
    const currentPosition = window.scrollY;
    if (currentPosition > 0) {
      window.requestAnimationFrame(scrollToTop);
      window.scrollTo(0, currentPosition - currentPosition / 8);
    }
  }
  return (
    <Box>
      <Grid container spacing={3} sx={styleGridContainer}>
        <Grid item xs={12} md={6}>
          <Box sx={styleBoxImage}>
            <img src={product.imgUrl} style={styleImage} />
          </Box>
        </Grid>

        <Grid item xs={12} md={6}>
          <Typography variant="h1" sx={styleTextName}>{product.name}</Typography>
          <Box sx={styleBoxBrand}>
            <Typography>Brand: Xiaomi</Typography>
          </Box>
          <Box sx={styleBoxRate}>
            <Typography>Rated:</Typography>
            <Rating defaultValue={4} sx={{ fontSize: "1.25rem" }} readOnly />
            <Typography>(50)</Typography>
          </Box>
          <Box sx={styleBoxPrice}>
            <Typography variant="h2" sx={styleTextPrice}>{product.price} US$</Typography>
            <Typography >Stock Available</Typography>
          </Box>
          {
            quantity ?
              <Counter
                counterQuantityProduct={quantity}
                product={product}
                addToCart={addToCart}
                removeProductItemQuantity={removeProductItemQuantity}
              />
              :
              <Button variant="contained" sx={styleButtonAddToCart} onClick={() => addToCart(product)}>Add To Cart</Button>
          }
        </Grid>
      </Grid>
    </Box>
  )
}

export default Index
