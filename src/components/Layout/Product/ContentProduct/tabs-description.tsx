import React, { useState } from "react"
import Box from "@mui/material/Box";
import TabContext from "@mui/lab/TabContext";
import TabList from "@mui/lab/TabList";
import { Tab, Typography } from "@mui/material";
import { TabPanel } from "@mui/lab";
import Avatar from "@mui/material/Avatar";
import Rating from "@mui/lab/Rating";

interface Reviewer {
    imgUrl: string
    name: string
    content: string
}

const TabsDescription = () => {
    const reviews: Reviewer[] = [
        { imgUrl: "https://bazaar.ui-lib.com/assets/images/faces/7.png", name: "Jannie Schumm", content: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Varius massa id ut mattis. Facilisis vitae gravida egestas ac account." },
        { imgUrl: "https://bazaar.ui-lib.com/assets/images/faces/6.png", name: "Joe Kenan", content: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Varius massa id ut mattis. Facilisis vitae gravida egestas ac account." },
        { imgUrl: "https://bazaar.ui-lib.com/assets/images/faces/8.png", name: "Jenifer Tulio", content: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Varius massa id ut mattis. Facilisis vitae gravida egestas ac account." }
    ]

    const styleText = {
        fontWeight: 600,
        fontSize: "0.8rem"
    }

    const styleTab = {
        minHeight: 40,
        textTransform: "none",
        color: "#D23F57",
    }

    const tabIndicatorStyle = {
        backgroundColor: "#D23F57"
    };

    const textDescription = <Typography sx={styleText}>Description</Typography>
    const textReview = <Typography sx={styleText}>Review</Typography>

    const styleTextSpecification = {
        mb: "16px",
        fontSize: 20,
        fontWeight: 700,
    }

    const styleBoxReviewer = {
        width: 600,
        mb: "32px"
    }

    const styleBoxInfoViewer = {
        display: "flex",
        mb: "16px"
    }

    const styleAvatar = {
        width: 48,
        height: 48 
    }

    const styleTextName = {
        mb: "4px",
        fontSize: 16,
        fontWeight: 600
    }

    const [value, setValue] = useState<string>("1");

    const handleChange = (event: React.SyntheticEvent, newValue: string) => {
        setValue(newValue);
    };

    const styleTabDescription = {
        color: value === "1" ? "#D23F57" : "#373F50"
    }

    const styleTabReview = {
        color: value === "2" ? "#D23F57" : "#373F50"
    }

    return (
        <TabContext value={value}>
            <Box sx={{ borderBottom: 1, borderColor: "divider", mt: "80px", mb: "24px" }}>
                <TabList onChange={handleChange} TabIndicatorProps={{ style: tabIndicatorStyle }}>
                    <Tab label={textDescription} value="1" style={styleTabDescription} sx={styleTab} />
                    <Tab label={textReview} value="2" style={styleTabReview} sx={styleTab} />
                </TabList>
            </Box>
            <TabPanel value="1" sx={{ p: 0 }}>
                <Box>
                    <Typography variant="h3" sx={styleTextSpecification}>Specification:</Typography>
                    <Box>
                        Brand: Beats<br />
                        Model: S450<br />
                        Wireless Bluetooth Headset<br />
                        FM Frequency Response: 87.5 – 108 MHz<br />
                        Feature: FM Radio, Card Supported (Micro SD / TF)<br />
                        Made in China<br />
                    </Box>
                </Box>
            </TabPanel>
            <TabPanel value="2" sx={{ p: 0 }}>
                <Box>
                    {reviews.map((reviewItem, index) => {
                        return (
                            <Box key={`${reviewItem.name}-${index}`} sx={styleBoxReviewer}>
                                <Box sx={styleBoxInfoViewer}>
                                    <Avatar src={reviewItem.imgUrl} sx={styleAvatar}/>
                                    <Box sx={{ ml: "16px" }}>
                                        <Typography variant="h5" sx={styleTextName}>{reviewItem.name}</Typography>
                                        <Box display="flex">
                                            <Rating defaultValue={5} sx={{ fontSize: "1.25rem" }} readOnly />
                                            <Typography variant="h6" sx={{ mx: "10px", fontSize: 14, fontWeight: 600 }}>4.7</Typography>
                                            <Typography component={"span"} sx={{ fontSize: 14, lineHeight: 1.6, letterSpacing: 0 }}>2.2 years ago</Typography>
                                        </Box>
                                    </Box>
                                </Box>

                                <Typography sx={{ fontSize: 14 }}>{reviewItem.content}</Typography>
                            </Box>
                        )
                    })}
                </Box>
            </TabPanel>
        </TabContext>
    )
}

export default TabsDescription
