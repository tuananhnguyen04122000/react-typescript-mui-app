import { Box, Container } from '@mui/system'
import React, { useEffect, useState } from 'react'
import ProductPhone from './product-phone'
import { IProductFlashDeal as IProductPhone } from "../FlashDeals/index"
import BrandsPhone from './brands-phone'
import Shops from './shops'
import axios from 'axios'

export interface IBrandsPhone {
    imgUrl?: string
    name?: string
}

export interface IShop {
    imgUrl?: string
    name?: string
}

const Index = () => {
    const brandsPhones: IBrandsPhone[] = [
        {
            imgUrl: "https://bazaar.ui-lib.com/_next/image?url=%2Fassets%2Fimages%2Fbrands%2Fapple.png&w=32&q=75", name: "Apple"
        },
        {
            imgUrl: "https://bazaar.ui-lib.com/_next/image?url=%2Fassets%2Fimages%2Fbrands%2Fdell.png&w=32&q=75", name: "Dell"
        },
        {
            imgUrl: "https://bazaar.ui-lib.com/_next/image?url=%2Fassets%2Fimages%2Fbrands%2Fxiaomi.png&w=32&q=75", name: "Xiaomi"
        },
        {
            imgUrl: "https://bazaar.ui-lib.com/_next/image?url=%2Fassets%2Fimages%2Fbrands%2Fasus.png&w=32&q=75", name: "Asus"
        },
        {
            imgUrl: "https://bazaar.ui-lib.com/_next/image?url=%2Fassets%2Fimages%2Fbrands%2Fsony.png&w=32&q=75", name: "Sony"
        },
        {
            imgUrl: "https://bazaar.ui-lib.com/_next/image?url=%2Fassets%2Fimages%2Fbrands%2Facer.png&w=32&q=75", name: "Acer"
        }
    ]

    const shops: IShop[] = [
        {
            imgUrl: "https://bazaar.ui-lib.com/_next/image?url=%2Fassets%2Fimages%2Fshops%2Fherman%20miller.png&w=32&q=75", name: "Keyboard Kiosk",
        },
        {
            imgUrl: "https://bazaar.ui-lib.com/_next/image?url=%2Fassets%2Fimages%2Fshops%2Fotobi.png&w=32&q=75", name: "Anytime Buys"
        },
        {
            imgUrl: "https://bazaar.ui-lib.com/_next/image?url=%2Fassets%2Fimages%2Fshops%2Fhatil.png&w=32&q=75", name: "Word Wide Wishes"
        },
        {
            imgUrl: "https://bazaar.ui-lib.com/_next/image?url=%2Fassets%2Fimages%2Fshops%2Fsteelcase.png&w=32&q=75", name: "Cybershop"
        }
    ]

    // const productPhones: IProductPhone[] = [
    //     {
    //         "imgUrl": "https://bazaar.ui-lib.com/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FElectronics%2F29.AppleEarphones.png&w=1920&q=75", "name": "Mapple Earphones", "price": 187, "oldPrice": 199, "discount": 6, "id": "32"
    //     },
    //     {
    //         "imgUrl": "https://bazaar.ui-lib.com/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FElectronics%2F30.Nokiaandroidone.png&w=1920&q=75", "name": "Lokia android one", "price": 111, "oldPrice": 122, "discount": 9, "id": "33"
    //     },
    //     {
    //         "imgUrl": "https://bazaar.ui-lib.com/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FElectronics%2F31.Symphonlights.png&w=1920&q=75", "name": "Xymphone lights", "price": 229, "oldPrice": 255, "discount": 1, "id": "34"
    //     },
    //     {
    //         "imgUrl": "https://bazaar.ui-lib.com/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FElectronics%2F32.iphone7.png&w=1920&q=75", "name": "Lphone 7", "price": 103, "oldPrice": 109, "discount": 5, id: "35"
    //     },
    //     {
    //         "imgUrl": "https://bazaar.ui-lib.com/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FElectronics%2F33.beatswirelessearphones.png&w=1920&q=75", "name": "Ceats wireless earphones", price: 171, "oldPrice": 181, "discount": 5, "id": "36"
    //     },
    //     {
    //         "imgUrl": "https://bazaar.ui-lib.com/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FElectronics%2F34.HTC2018.png&w=1920&q=75", "name": "HPC 2018", "price": 140, "oldPrice": 154, "discount": 9, id: "37"
    //     },
    //     {
    //         "imgUrl": "https://bazaar.ui-lib.com/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FElectronics%2F35.beatsbluetoothearpohones.png&w=1920&q=75", "name": "Xeats bluetooth earphones", "price": 134, "oldPrice": 148, "discount": 9, "id": "38"
    //     },
    //     {
    //         "imgUrl": "https://bazaar.ui-lib.com/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FElectronics%2F36.sbsWirelessEarphones.png&w=1920&q=75", "name": "sbs Wireless Earphones", "price": 145, "oldPrice": 160, "discount": 9, "id": "39"
    //     },
    //     {
    //         "imgUrl": "https://bazaar.ui-lib.com/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FFashion%2FAccessories%2F1.SilverCap.png&w=1920&q=75", "name": "Silver Cap", "price": 225, "oldPrice": 237, "discount": 5, "id": "40"
    //     },
    // ]

    const [showBrandsPhone, setshowBrandsPhone] = useState<boolean>(true);

    const toggleComponents = () => {
        setshowBrandsPhone(!showBrandsPhone);
    };

    const [productPhones, setProductPhones] = useState<IProductPhone[]>([])

    useEffect(() => {
        fetchData()
    }, [])

    async function fetchData() {
        try {
            await axios.get('https://6402f090f61d96ac4873acc5.mockapi.io/api/product').then((result) => {
                setProductPhones(result.data[0].mobilePhones);
            });

        } catch (errors) {
            console.log(errors)
        }
    }

    return (
        <Container maxWidth={false} sx={{ width: 1280, height: 1330.97, mb: "80px" }}>
            <Box sx={{ display: "flex", gap: "1.75rem" }}>
                {showBrandsPhone ? 
                        <BrandsPhone brandsPhones={brandsPhones} handleClick={toggleComponents} />
                        :
                        <Shops shops={shops} handleClick={toggleComponents} />
                }
                <ProductPhone productPhones={productPhones} />
            </Box>
        </Container>
    )
}

export default Index
