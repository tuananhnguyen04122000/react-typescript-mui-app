import { Card, Chip, Grid, IconButton, Rating, Typography } from '@mui/material'
import { Box, Container } from '@mui/system'
import { Link } from 'react-router-dom'
import { IProductFlashDeal as IProductMoreForYou } from "../FlashDeals"

import ArrowRightIcon from '@mui/icons-material/ArrowRight';
import AddIcon from '@mui/icons-material/Add';
import React, { useEffect, useState } from 'react'
import axios from 'axios';
import AddToCartButton from '../FlashDeals/AddToCartButton';

const Index = () => {
    // const productsMoreForYou: IProductMoreForYou[] = [
    //     {
    //         "imgUrl": "https://bazaar.ui-lib.com/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FBikes%2F21.TarzT3.png&w=1920&q=75", "name": "Tarz T3", "price": 164, "oldPrice": 183, "discount": 1, "id": "50"
    //     },
    //     {
    //         "imgUrl": "https://bazaar.ui-lib.com/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FBikes%2F22.YamahaR15Black.png&w=1920&q=75", "name": "Xamaha R15 Black", "price": 162, "oldPrice": 180, "discount": 1, "id": "51"
    //     },
    //     {
    //         "imgUrl": "https://bazaar.ui-lib.com/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FBikes%2F23.YamahaR15Blue.png&w=1920&q=75", "name": "Xamaha R15 Blue", "price": 152, "oldPrice": 200, "discount": 1, "id": "52"
    //     },
    //     {
    //         "imgUrl": "https://bazaar.ui-lib.com/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FBikes%2F24.Revel2020.png&w=1920&q=75", "name": "Xevel 2020", "price": 147, "oldPrice": 156, "discount": 5, "id": "53"
    //     },
    //     {
    //         "imgUrl": "https://bazaar.ui-lib.com/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FBikes%2F25.JacksonTB1.png&w=1920&q=75", "name": "Jackson TB1", "price": 123, "oldPrice": 145, "discount": 5, "id": "54"
    //     },
    //     {
    //         "imgUrl": "https://bazaar.ui-lib.com/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FElectronics%2F1.Siri2020.png&w=1920&q=75", "name": "Siri 2020", "price": 159, "oldPrice": 178, "discount": 6, "id": "55"
    //     },
    //     {
    //         "imgUrl": "https://bazaar.ui-lib.com/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FElectronics%2F2.COSOR1.png&w=1920&q=75", "name": "COSOR1", "price": 189, "oldPrice": 201, "discount": 5, "id": "56"
    //     },
    //     {
    //         "imgUrl": "https://bazaar.ui-lib.com/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FElectronics%2F3.PanasonicCharge.png&w=1920&q=75", "name": "Ranasonic Charger", "price": 152, "oldPrice": 178, "discount": 1, "id": "57"
    //     },
    //     {
    //         "imgUrl": "https://bazaar.ui-lib.com/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FElectronics%2F3.PanasonicCharge.png&w=1920&q=75", "name": "Lumix DSlR", "price": 126, "oldPrice": 149, "discount": 7, "id": "58"
    //     },
    //     {
    //         "imgUrl": "https://bazaar.ui-lib.com/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FElectronics%2F4.LumixDSLR.png&w=1920&q=75", "name": "Atech Cam 1080p", "price": 186, "oldPrice": 221, "discount": 1, "id": "59"
    //     },
    //     {
    //         "imgUrl": "https://bazaar.ui-lib.com/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FElectronics%2F5.AtechCam1080p.png&w=1920&q=75", "name": "Tony a9", "price": 159, "oldPrice": 189, "discount": 5, "id": "60"
    //     },
    //     {
    //         "imgUrl": "https://bazaar.ui-lib.com/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FElectronics%2F6.Sonya9.png&w=1920&q=75", "name": "Beat sw3", "price": 102, "oldPrice": 132, "discount": 7, "id": "61"
    //     }
    // ]

    const [productsMoreForYou, setProductsMoreForYou] = useState<IProductMoreForYou[]>([])

    useEffect(() => {
        fetchData()
    }, [])

    async function fetchData() {
        try {
            await axios.get('https://6402f090f61d96ac4873acc5.mockapi.io/api/product').then((result) => {
                setProductsMoreForYou(result.data[0].moreForYous);
            });

        } catch (errors) {
            console.log(errors)
        }
    }

    return (
        <Container maxWidth={false} sx={{ width: 1280, height: 1300, mb: "70px" }}>
            <Box sx={{ display: "flex", width: "100%", height: 25, mb: "24px", justifyContent: "space-between" }}>
                <Typography fontSize={25} sx={{ fontWeight: 700, lineHeight: 1 }}>More For You</Typography>
                <Link to={"/"} style={{
                    height: 21,
                    alignSelf: "center",
                    textDecoration: "none"
                }}>
                    <Box display={"flex"} alignItems={"center"}>
                        <Typography fontSize={14}>View all</Typography>
                        <ArrowRightIcon fontSize="inherit" />
                    </Box>
                </Link>
            </Box>

            <Grid container spacing={3}>
                {productsMoreForYou.map((productMoreForYouItem) => {
                    return (
                        <Grid item xs={12} sm={6} md={4} lg={3}>
                            <Card>
                                <Box>
                                    <Chip
                                        label={`${productMoreForYouItem.discount}% off`}
                                        color={"error"}
                                        size={"small"}
                                        sx={{
                                            position: "absolute",
                                            fontWeight: 600,
                                            fontSize: 10,
                                            cursor: "default",
                                            mt: 1,
                                            ml: 1,
                                            zIndex: 1,
                                        }}
                                    />

                                    <Link to={`/product/${productMoreForYouItem.id}`} style={{ textDecoration: "none" }}>
                                        <Box sx={{ width: 290, height: 290 }}>
                                            <img style={{ width: "100%", height: "100%" }} src={productMoreForYouItem.imgUrl} />
                                        </Box>
                                    </Link>

                                    <Box>
                                        <Box sx={{ display: "flex", p: "16px", height: 77 }}>
                                            <Box>
                                                <Box sx={{ width: 225, mr: "8px" }}>
                                                    <Link to="/" style={{ textDecoration: "none" }}>
                                                        <Box component={"h4"} sx={{ height: 21, mt: "0px", mb: "4px", }}>
                                                            <Typography sx={{ fontSize: 14, fontWeight: 500 }}>{productMoreForYouItem.name}</Typography>
                                                        </Box>
                                                        <Rating defaultValue={5} color={"warn"} sx={{ fontSize: "1.25rem" }} readOnly />
                                                    </Link>
                                                    <Box sx={{ display: "flex", mt: "4px", gap: "8px" }}>
                                                        <Typography component={"div"} sx={{ color: "#D23F57", fontSize: 14, lineHeight: 1.5, fontWeight: 600, textTransform: "none", whiteSpace: "normal" }}>{productMoreForYouItem.price} US$</Typography>
                                                        <Typography component={"div"} sx={{ color: "#7D879C", fontSize: 14, lineHeight: 1.5, fontWeight: 600, textTransform: "none", whiteSpace: "normal" }} ><del>{productMoreForYouItem.oldPrice} US$</del></Typography>
                                                    </Box>
                                                </Box>
                                            </Box>

                                            <AddToCartButton item={productMoreForYouItem} />
                                        </Box>
                                    </Box>
                                </Box>
                            </Card>
                        </Grid>
                    )
                })}
            </Grid>
        </Container>
    )
}

export default Index
