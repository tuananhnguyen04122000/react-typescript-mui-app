import { Container, Grid } from '@mui/material'
import React from 'react'
import { Link } from 'react-router-dom'

const Index = () => {
    return (
        <Container maxWidth={false} sx={{ width: 1280, height: 385.53, mb: "70px" }}>
            <Grid container spacing={3} sx={{ mt: "-24px", ml: "-24px" }}>
                <Grid item xs={12} md={4}>
                    <Link to={"/"} style={{ width: 394.663, height: 350.375 }}>
                        <img style={{ width: "100%", height: "100%" }} src="https://bazaar.ui-lib.com/_next/image?url=%2Fassets%2Fimages%2Fbanners%2Fbanner-1.png&w=1920&q=75" />
                    </Link>
                </Grid>

                <Grid item xs={12} md={8}>
                    <Link to={"/"} style={{ width: 813.33, height: 352.09 }}>
                        <img style={{ width: "100%", height: "100%" }} src="https://bazaar.ui-lib.com/_next/image?url=%2Fassets%2Fimages%2Fbanners%2Fbanner-2.png&w=1920&q=75" />
                    </Link>
                </Grid>
            </Grid>
        </Container>
    )
}

export default Index
