import { Card, Grid, IconButton, Typography } from '@mui/material'
import React, { FC } from 'react'
import { IService } from '.'

interface PropsServices {
    services: IService[]
}

const Services: FC<PropsServices> = (props) => {
    const { services } = props

    return (
        <Grid container spacing={3} sx={{ mt: "-24px", ml: "-24px" }}>
            {services.map((serviceItem, index) => {
                return (
                    <Grid key={`${serviceItem.title}-${index}`} item xs={12} md={6} lg={3} >
                        <Card sx={{ display: "flex", p: "48px", flexDirection: "column", alignItems: "center" }}>
                            <IconButton sx={{ width: 64, height: 64, bgcolor: "#F3F5F9", p: "8px", fontSize: "1.75rem" }}>
                                {serviceItem.icon}
                            </IconButton>

                            <Typography
                                variant="h4"
                                sx={{
                                    mb: "10px",
                                    mt: "20px",
                                    fontSize: 17,
                                    fontWeight: 600,
                                    lineHeight: 1.5,
                                    textAlign: "center",
                                    textTransform: "none",
                                    whiteSpace: "normal"
                                }}>{serviceItem.title}</Typography>

                            <Typography
                                component={"span"}
                                sx={{
                                    color: "#7D879C",
                                    fontSize: 14,
                                    fontWeight: 400,
                                    textAlign: "center",
                                    textTransform: "none",
                                    whiteSpace: "normal"
                                }}>{serviceItem.content}</Typography>
                        </Card>
                    </Grid>
                )
            })}
        </Grid>
    )
}

export default Services
