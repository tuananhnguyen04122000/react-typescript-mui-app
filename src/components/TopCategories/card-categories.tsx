import { Height } from '@mui/icons-material';
import { CardContent, Chip, Typography } from '@mui/material';
import Card from '@mui/material/Card';
import React, { FC } from 'react'
import Slider from 'react-slick';
import { IProductCategories } from '.'
import SampleNextArrow from './top-categories-arrow-button-custom/SampleNextArrow';
import SamplePrevArrow from './top-categories-arrow-button-custom/SamplePrevArrow';

interface CardCategoriesProps {
    productCategories: IProductCategories[]
}
const CardCategories: FC<CardCategoriesProps> = (props) => {
    const { productCategories } = props;

    const settings = {
        dots: false,
        infinite: true,
        speed: 500,
        slidesToShow: 3,
        slidesToScroll: 1,
        nextArrow: <SampleNextArrow />,
        prevArrow: <SamplePrevArrow />,
        swipeToSlide: true,
        touchThreshold: 100,
        className: "slick-track-categories"
    };

    const styleImage = {
        width: "100%",
        height: "100%",
        borderRadius: "8px"
    }

    const styleCard = {
        maxWidth: 394.66,
        height: 152,
        borderRadius: "8px"
    }

    const styleCardContent = {
        height: 120,
        ":hover": {
            filter: 'brightness(70%)',
            transition: "filter 0.4s"
        }
    }

    const styleChipName = {
        position: "absolute",
        mt: 2,
        ml: 2,
        color: "white",
        bgcolor: "#0F3460",
        zIndex: 2
    }

    const styleChipOrders = {
        position: "absolute",
        mt: 2,
        ml: 28,
        color: "black",
        bgcolor: "rgba(0, 0, 0, 0.08)",
        zIndex: 2
    }

    return (
        <Slider {...settings} >
            {productCategories.map((productCategoriesItem, index) => {
                return (
                    <Card key={`${productCategoriesItem}-${index}`} sx={styleCard}>
                        <CardContent sx={styleCardContent}>
                            <Chip
                                label={<Typography fontSize={"10px"} px={"8px"}>{productCategoriesItem.name}</Typography>}
                                size={"small"}
                                sx={styleChipName}
                            />
                            <Chip
                                label={<Typography fontSize={"10px"} px={"8px"}>{productCategoriesItem.ordersThisWeek}k orders this week</Typography>}
                                size={"small"}
                                sx={styleChipOrders}
                            />
                            <img className="image-categories" style={styleImage} src={`${productCategoriesItem.imgUrl}`} />
                        </CardContent>
                    </Card>
                )
            })}
        </Slider>
    )
}

export default CardCategories

