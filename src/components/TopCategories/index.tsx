import Box from '@mui/material/Box'
import Container from '@mui/material/Container'
import Typography from '@mui/material/Typography'
import WidgetsIcon from '@mui/icons-material/Widgets';
import ArrowRightIcon from '@mui/icons-material/ArrowRight';
import React, { useEffect, useState } from 'react'
import { Link } from 'react-router-dom';
import CardCategories from './card-categories';

export interface IProductCategories {
    name?: string
    imgUrl?: string
    ordersThisWeek?: number
}

const Index = () => {
  const productCategories: IProductCategories[] = [
    {
      name: "Headphone",
      imgUrl: "https://bazaar.ui-lib.com/_next/image?url=%2Fassets%2Fimages%2Fbanners%2Fcategory-1.png&w=1920&q=75",
      ordersThisWeek: 3
    },
    {
      name: "Watch",
      imgUrl: "https://bazaar.ui-lib.com/_next/image?url=%2Fassets%2Fimages%2Fbanners%2Fcategory-2.png&w=1920&q=75",
      ordersThisWeek: 4
    },
    {
      name: "Sunglass",
      imgUrl: "https://bazaar.ui-lib.com/_next/image?url=%2Fassets%2Fimages%2Fbanners%2Fcategory-3.png&w=1920&q=75",
      ordersThisWeek: 5
    },
    {
      name: "Headphone",
      imgUrl: "https://bazaar.ui-lib.com/_next/image?url=%2Fassets%2Fimages%2Fbanners%2Fcategory-1.png&w=1920&q=75",
      ordersThisWeek: 3
    },
    {
      name: "Watch",
      imgUrl: "https://bazaar.ui-lib.com/_next/image?url=%2Fassets%2Fimages%2Fbanners%2Fcategory-2.png&w=1920&q=75",
      ordersThisWeek: 4
    }
  ]


  // const [productCategories, setProductCategories] = useState<IProductCategories[]>([])

  // useEffect(() => {
  //     fetch("https://6402f090f61d96ac4873acc5.mockapi.io/api/top-categories")
  //         .then(response => response.json())
  //         .then(data => setProductCategories(data))
  // }, [])

  return (
    <Box height={217} mb={"60px"}>
      <Container maxWidth={false} sx={{ width: 1280, height: 217 }}>
        <Box sx={{ display: "grid", width: 1232, height: 25, mb: "24px", gridTemplateAreas: `"top-categories view-all"`, zIndex: 5 }}>
          <Box sx={{
            display: "flex",
            gridArea: "top-categories",
            height: 25,
            alignItems: "center",
            gap: 1
          }}>
            <WidgetsIcon color="error" />
            <Typography fontSize={25} sx={{ fontWeight: 700, lineHeight: 1 }}>Top Categories</Typography>
          </Box>
          <Link to={"/"} style={{
            gridArea: "view-all",
            height: 21,
            alignSelf: "center",
            justifySelf: "end",
            textDecoration: "none"
          }}>
            <Box display={"flex"} alignItems={"center"}>
              <Typography fontSize={14}>View all</Typography>
              <ArrowRightIcon fontSize="inherit" />
            </Box>
          </Link>
        </Box>

        <CardCategories productCategories={productCategories} />
      </Container>
    </Box>
  )
}

export default Index
