import { Card, CardContent, Typography } from '@mui/material'
import Box from '@mui/material/Box'
import Grid from '@mui/material/Grid'
import ArrowRightIcon from '@mui/icons-material/ArrowRight';
import StarPurple500Icon from '@mui/icons-material/StarPurple500';
import React, { FC } from 'react'
import { Link } from 'react-router-dom'
import { IProductFeaturedBrand } from '.'

interface FeaturedBrandsProps {
  productFeaturedBrands: IProductFeaturedBrand[]
}

const FeaturedBrands: FC<FeaturedBrandsProps> = (props) => {
  const { productFeaturedBrands } = props;
  
  return (
    <Grid item xs={12} lg={6}>
      <Box sx={{ display: "grid", width: 600, height: 25, mb: "24px", gridTemplateAreas: `"featured-brands view-all"` }}>
        <Box sx={{
          display: "flex",
          gridArea: "featured-brands",
          alignItems: "center",
          gap: "8px"
        }}>
          <StarPurple500Icon sx={{ color: "#fab400" }} />
          <Typography fontSize={25} sx={{ fontWeight: 700, lineHeight: 1 }}>Featured Brands</Typography>
        </Box>
        <Link to={"/"} style={{
          gridArea: "view-all",
          height: 21,
          alignSelf: "center",
          justifySelf: "end",
          textDecoration: "none"
        }}>
          <Box display={"flex"} alignItems={"center"}>
            <Typography fontSize={14}>View all</Typography>
            <ArrowRightIcon fontSize="inherit" />
          </Box>
        </Link>
      </Box>

      <Card>
        <CardContent sx={{ display: "flex", width: 568, height: 204 }}>
          <Grid container spacing={4} sx={{ height: 236, mt: "-32px", ml: "-32px" }}>
            {productFeaturedBrands?.map((productFeaturedBrandItem, productFeaturedBrandIndex) => {
              return (
                <Grid item xs={12} sm={6} key={`${productFeaturedBrandItem}-${productFeaturedBrandIndex}`}>
                  <Link to={`/`} style={{ textDecoration: "none" }}>
                    <Box sx={{ ":hover": { filter: 'brightness(70%)', transition: "filter 0.4s" }}}>
                      <img src={productFeaturedBrandItem.imgUrl} style={{ width: 268, height: 180.375, paddingBottom: "8px", borderRadius: 8 }} />
                    </Box>
                    <Box component={"h4"} sx={{ height: 21, mt: "0px", mb: "4px" }}>
                      <Typography sx={{ fontSize: 14, fontWeight: 500 }}>{productFeaturedBrandItem.name}</Typography>
                    </Box>
                  </Link>
                </Grid>
              )
            })}
          </Grid>
        </CardContent>
      </Card>
    </Grid>
  )
}

export default FeaturedBrands
