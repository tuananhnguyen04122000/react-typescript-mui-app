import Box from '@mui/material/Box'
import Grid from '@mui/material/Grid'
import Typography from '@mui/material/Typography'
import WorkspacePremiumIcon from '@mui/icons-material/WorkspacePremium';
import ArrowRightIcon from '@mui/icons-material/ArrowRight';
import React, { FC } from 'react'
import { Link } from 'react-router-dom'
import { IProductTopRating } from '.';
import { Card, CardContent, Rating } from '@mui/material';

interface TopRatingProps {
  productTopRatings: IProductTopRating[]
}

const TopRating: FC<TopRatingProps> = (props) => {
  const { productTopRatings } = props

  return (
    <Grid item xs={12} lg={6}>
      <Box sx={{ display: "grid", width: 600, height: 25, mb: "24px", gridTemplateAreas: `"top-ratings view-all"` }}>
        <Box sx={{
          display: "flex",
          gridArea: "top-ratings",
          alignItems: "center",
          gap: "8px"
        }}>
          <WorkspacePremiumIcon sx={{ color: "#fab400"}} />
          <Typography fontSize={25} sx={{ fontWeight: 700, lineHeight: 1 }}>Top Ratings</Typography>
        </Box>
        <Link to={"/"} style={{
          gridArea: "view-all",
          height: 21,
          alignSelf: "center",
          justifySelf: "end",
          textDecoration: "none"
        }}>
          <Box display={"flex"} alignItems={"center"}>
            <Typography fontSize={14}>View all</Typography>
            <ArrowRightIcon fontSize="inherit" />
          </Box>
        </Link>
      </Box>
      
      <Card>
        <CardContent sx={{ display: "flex", width: 568, height: 204 }}>
          <Grid container spacing={4} sx={{ height: 236, mt: "-32px", ml: "-32px" }}>
            {productTopRatings.map((productTopRatingItem, index) => {
              return (
                <Grid key={`${productTopRatingItem.name}-${index}`} item xs={6} sm={6} md={3}>
                  <Link to={`/product/${productTopRatingItem.id}`} style={{ textDecoration: "none" }}>
                    <Box sx={{ ":hover": { filter: 'brightness(70%)', transition: "filter 0.4s" }}}>
                      <img src={productTopRatingItem.imgUrl} style={{ width: 118, height: 118, marginBottom: "16px" }} />
                    </Box>
                    <Box sx={{ display: "flex", mb: "4px" }}>
                      <Rating defaultValue={5} color={"warn"} readOnly sx={{ fontSize: "1.25rem" }} />
                      <Box component={"small"}>
                        <Typography sx={{ fontSize: 12, lineHeight: 1.5, fontWeight: 600, pl: "4px", textTransform: "none", whiteSpace: "normal" }}>(0)</Typography>
                      </Box>
                    </Box>
                    <Box component={"h4"} sx={{ height: 21, mt: "0px", mb: "4px", textAlign: "center" }}>
                      <Typography sx={{ fontSize: 14, fontWeight: 500 }}>{productTopRatingItem.name}</Typography>
                    </Box>
                    <Box component={"h4"} sx={{ height: 21, mt: "0px", mb: "0px", textAlign: "center" }}>
                      <Typography sx={{ color: "#D23F57", fontSize: 14, lineHeight: 1.5, fontWeight: 450, textTransform: "none", whiteSpace: "normal" }}>{productTopRatingItem.price},00 US$</Typography>
                    </Box>
                  </Link>
                </Grid>
              )
            })}
          </Grid>
        </CardContent>
      </Card>


    </Grid>
  )
}

export default TopRating
