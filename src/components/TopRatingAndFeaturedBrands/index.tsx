import Box from '@mui/material/Box'
import Container from '@mui/material/Container'
import Grid from '@mui/material/Grid'
import axios from 'axios'
import { Console } from 'console'
import React, { useEffect, useState } from 'react'
import FeaturedBrands from './FeaturedBrands'
import TopRatings from './TopRatings'
import TopRating from './TopRatings'

export interface IProductTopRating {
  imgUrl?: string
  name?: string
  price?: number
  id: string
}

export interface IProductFeaturedBrand {
  imgUrl?: string
  name?: string
  id?: number
}

const Index = () => {
  // const productTopRatings: IProductTopRating[] = [
  //   {
  //     imgUrl: "https://bazaar.ui-lib.com/_next/image?url=%2Fassets%2Fimages%2Fproducts%2Fcamera-1.png&w=1920&q=75", name: "Camera", price: 3.300, id: ""
  //   },
  //   {
  //     imgUrl: "https://bazaar.ui-lib.com/_next/image?url=%2Fassets%2Fimages%2Fproducts%2Fshoes-2.png&w=1920&q=75", name: "Shoe", price: 400, id: ""
  //   },
  //   {
  //     imgUrl: "https://bazaar.ui-lib.com/_next/image?url=%2Fassets%2Fimages%2Fproducts%2Fmobile-1.png&w=1920&q=75", name: "Phone", price: 999, id: ""
  //   },
  //   {
  //     imgUrl: "https://bazaar.ui-lib.com/_next/image?url=%2Fassets%2Fimages%2Fproducts%2Fwatch-1.png&w=1920&q=75", name: "Watch", price: 999, id: ""
  //   }
  // ]


  // const productFeaturedBrands: IProductFeaturedBrand[] = [
  //   {
  //     imgUrl: "https://bazaar.ui-lib.com/_next/image?url=%2Fassets%2Fimages%2Fproducts%2Flondon-britches.png&w=1920&q=75", name: "London Britches"
  //   },
  //   {
  //     imgUrl: "https://bazaar.ui-lib.com/_next/image?url=%2Fassets%2Fimages%2Fproducts%2Fjim%20and%20jiko.png&w=1920&q=75", name: "Jim & Jago", 
  //   },
  // ]

  const [productFeaturedBrands, setProductFeaturedBrands] = useState<IProductFeaturedBrand[]>([])
  const [productTopRatings, setProductTopRatings] = useState<IProductTopRating[]>([])

  useEffect(() => {
    fetchData()
  }, [])

  async function fetchData() {
    try {
     await axios.get('https://6402f090f61d96ac4873acc5.mockapi.io/api/product').then((result)=>{
       setProductFeaturedBrands(result.data[0].featuredBrands);
       setProductTopRatings(result.data[0].topRatings)
     });
    
    } catch (errors) {
      console.log(errors)
    }
  }
  
  return (
    <Box mb={"60px"}>
      <Container maxWidth={false} sx={{ width: 1280, height: 322.375 }}>
        <Grid container spacing={4} sx={{ width: 1264, height: "100%" }}>
          <TopRatings productTopRatings={productTopRatings} />
        <FeaturedBrands productFeaturedBrands={productFeaturedBrands} />
        </Grid>
      </Container>
    </Box>
  )
}

export default Index
