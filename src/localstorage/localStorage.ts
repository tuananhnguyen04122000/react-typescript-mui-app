import { ICart } from "../recoil/Atoms";

export const saveCartToLocalStorage = (cart: ICart[]) => {
    localStorage.setItem('cart', JSON.stringify(cart));
  };