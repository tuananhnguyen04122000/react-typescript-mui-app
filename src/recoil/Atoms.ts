import { atom } from "recoil"
export interface IProduct {
    id?: string
    imgUrl?: string
    name?: string
    price: number
}

export interface ICart extends IProduct {
    quantity: number
    totalPriceQuantity: number
}

export const cartState = atom({
    key: "cartState",
    default: [] as ICart[]
})

export const shoppingBagState = atom({
    key: "shoppingBagState",
    default: 0
});

export const showCounterState = atom({
    key: "showCounterState",
    default: true
});

export const counterProduct = atom({
    key: "counterProduct",
    default: 0
})