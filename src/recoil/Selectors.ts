import { selector } from "recoil";
import { cartState, ICart } from './Atoms';

export const cartTotalPrice = selector({
    key: "cartTotalPrice",
    get: ({ get }) => {
        const cart: ICart[] = get(cartState);

        return cart.reduce((total: number, item: ICart) => {
            return total + item.totalPriceQuantity
        }, 0)
    }
})

export const cartTotalProduct = selector({
    key: "cartTotalProduct",
    get: ({ get }) => {
        const cart: ICart[] = get(cartState);

        return cart.length
    }
})